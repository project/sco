In case you wish to use the Scots translation, please join the Scots translation
team at http://localize.drupal.org/translate/languages/sco - from here it is
possible to export translations by use of the "Export" tab. This will both
provide you with the latest translation of what you request, as well as provide
you with more options with regards to how you get the translations delivered.

Feel free to report bugs with the translation at the bugtracker:
http://drupal.org/project/issues/sco
- or simply make a suggestion directly in the translation interface:
http://localize.drupal.org/translate/languages/sco/translate

Thank you. :)
